package ca.rbgproductions.axial.modules.calendar;

import java.util.Locale;
import org.joda.time.DateTime;

public class Calendar {

  private Locale   locale;
  private DateTime date;

  public Calendar() {
    locale = Locale.getDefault();
    date = new DateTime();
  }

  public String render() {
    StringBuilder calendar = new StringBuilder();

    formatMonthOfYearRow(date, calendar);
    formatDaysOfWeekRow(calendar);
    formatDaysOfMonthRows(date, calendar);

    calendar.append("</table>\n");

    return calendar.toString();
  }

  private static void formatDaysOfMonthRows(DateTime date,
      StringBuilder calendar) {
    // Calculate first day of week offset
    int offset = calculateOffset(date);
    // Render all table cells for days of the month
    for (int i = 1 - offset; i <= date.dayOfMonth().getMaximumValue();) {
      // Row declaration
      calendar.append("  <tr class=\"calendar-row\">\n");
      // Iterate over days of the week
      for (int j = 0; j < date.dayOfWeek().getMaximumValue(); j++) {
        // Do not append numbers if day is below 1 or above last day of
        // month
        if (i < 1 || i > date.dayOfMonth().getMaximumValue()) {
          // Append empty cell
          calendar.append("    <td class=\"calendar-cell\"></td>\n");
          i++;
        } else {
          // Append numbered cell with day of the month
          calendar.append(String.format(
              "    <td id=\"%d\" class=\"calendar-cell\">%d</td>\n", i, i));
          i++;
        }
      }
      // End of row
      calendar.append("  </tr>\n");
    }
  }

  private static int calculateOffset(DateTime date) {
    // first day of the week in month = day of the week - (day of today % 7)
    int offset = date.getDayOfMonth() % 7;
    offset = date.getDayOfWeek() - offset;

    // Has to be strictly positive
    if (offset < 0) {
      return offset + 7;
    } else {
      return offset;
    }
  }

  private static void formatDaysOfWeekRow(StringBuilder calendar) {
    // Row declaration
    calendar.append("  <tr id=\"days-of-week\">\n");

    // Sunday, Monday, Tuesday...
    for (int i = 1; i <= 7; i++) {
      calendar.append(String.format("    <td>%s</td>\n",
          parseDayOfWeek(i, true)));
    }
    calendar.append("  </tr>\n");
  }

  private static void formatMonthOfYearRow(DateTime date, StringBuilder calendar) {
    calendar
        .append(String
            .format(
                "<table class=\"calendar\">\n  <tr>\n    <th colspan=\"7\" id=\"month-of-year\">%s %d</th>\n  </tr>\n",
                parseMonthOfYear(date.getMonthOfYear(), false), date.getYear()));
  }

  private static String parseDayOfWeek(int dayOfWeek, boolean isShort) {
    if (isShort) {
      switch (dayOfWeek) {
        case 1:
          return "Mo";
        case 2:
          return "Tu";
        case 3:
          return "We";
        case 4:
          return "Th";
        case 5:
          return "Fr";
        case 6:
          return "Sa";
        case 7:
          return "Su";
      }
    } else {
      switch (dayOfWeek) {
        case 1:
          return "Monday";
        case 2:
          return "Tuesday";
        case 3:
          return "Wednesday";
        case 4:
          return "Thursday";
        case 5:
          return "Friday";
        case 6:
          return "Saturday";
        case 7:
          return "Sunday";
      }
    }
    return null;
  }

  private static String parseMonthOfYear(int monthOfYear, boolean isShort) {
    if (isShort) {
      switch (monthOfYear) {
        case 1:
          return "Jan";
        case 2:
          return "Feb";
        case 3:
          return "Mar";
        case 4:
          return "Apr";
        case 5:
          return "May";
        case 6:
          return "Jun";
        case 7:
          return "Jul";
        case 8:
          return "Aug";
        case 9:
          return "Sept";
        case 10:
          return "Oct";
        case 11:
          return "Nov";
        case 12:
          return "Dec";
      }
    } else {
      switch (monthOfYear) {
        case 1:
          return "January";
        case 2:
          return "February";
        case 3:
          return "March";
        case 4:
          return "April";
        case 5:
          return "May";
        case 6:
          return "June";
        case 7:
          return "July";
        case 8:
          return "August";
        case 9:
          return "September";
        case 10:
          return "October";
        case 11:
          return "November";
        case 12:
          return "December";
      }
    }
    return null;
  }

  public static void main(String[] args) {
    Calendar calendar = new Calendar();
    System.out.print(calendar.render());
  }
}
