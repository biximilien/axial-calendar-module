package ca.rbgproductions.axial.modules.calendar;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

public class Event {

    private String   name;
    private DateTime begin;
    private DateTime end;

    public Event(String name, String dateBegin, String dateEnd) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBegin() {
        return begin.toLocalDateTime().toString(null, null);
    }

    public void setBegin(String dateBegin) {
        DateTime dateTime = new DateTime();
        this.begin = null;
    }

}
